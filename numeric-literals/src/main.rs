fn main() 
{
  let three = 0b11; //Bin: 11
  let thirty = 0o36; //Oct: 36
  let three_hundred = 0x12C;    //Hex: 12C

  println!("{} \t {} \t {}", three, thirty, three_hundred);
  println!("{:b} \t {:b} \t {:b}", three, thirty, three_hundred);
  println!("{:o} \t {:o} \t {:o}", three, thirty, three_hundred);
  println!("{:x} \t {:x} \t {:x}", three, thirty, three_hundred);
  
  // let error = 5 + "5";
}