extern crate azul;

// Import file utilities
use std::fs::File;
use std::io::prelude::*;
//use std::io::BufReader;

// Import time utilities
// Import env utilities
use std::env;
// use std::path::Path;

// ?
//use azul::prelude::*;
// Import all widgets
//use azul::widgets::button;
//use azul::widgets::label;
use azul::{
    prelude::*,
    widgets::{button::Button, label::Label},
};

// Variables
struct CounterApplication {
    counter: usize,
}

// Update Counter function
fn update_counter(
    app_state: &mut AppState<CounterApplication>,
    _event: WindowEvent<CounterApplication>,
) -> UpdateScreen {
    app_state.data.modify(|state| state.counter += 1);
    UpdateScreen::Redraw
}

// Application Layout
impl Layout for CounterApplication {
    fn layout(&self, _info: WindowInfo<Self>) -> Dom<Self> {
        // Label to output counter value
        let label = Label::new(format!("{}", self.counter))
            .dom()
            .with_id("lbl");
        // Button to change counter value
        let button = Button::with_label("Update counter")
            .dom()
            .with_class("btn")
            .with_callback(On::MouseUp, Callback(update_counter));
            //.with_callback(On::MouseUp, Callback(start_timer));

        // Put the widgets in the main view
        Dom::new(NodeType::Div).with_child(label).with_child(button)
    }
}
/*
fn load_style() -> <'a>&str {
    //println!("In file {}", filename);

    let filename = "/home/simone/git/rust/azul_test/src/style.css";
    let mut f = File::open(filename).expect("file not found");

    let mut contents = String::new();
    f.read_to_string(&mut contents)
        .expect("something went wrong reading the file");

    &contents[..]
}
*/
fn main() 
{
    let mut path = env::current_dir().unwrap();
    //println!("The current directory is {:?}", path);
    path.push("src");
    path.push("style.css");
    // env::set_current_dir("/home/simone/git/rust/azul/counter/src/");
    // env::set_current_dir("../../src/");

    //let filename = "style.css";
    //let full_path = path + filename;
    let mut f = File::open(path).expect("file not found");

    let mut contents = String::new();
    f.read_to_string(&mut contents)
        .expect("something went wrong reading the file");

    //let css_string = load_style();
    //let css_string:azul::css::Css = Css::new_from_str(&contents[..]).unwrap();
    let final_css_string = Css::override_native(&contents[..]).unwrap();
    //let css_string = Css::new_from_str(&css_string);
    //let final_css_string:azul::css::Css = azul::css::Css::merge(&mut css_string, Css::native());

    // Initialize counter
    let app = App::new(CounterApplication { counter: 0 }, AppConfig::default());
    // Main loop
    app.run(Window::new(WindowCreateOptions::default(), final_css_string).unwrap())
        .unwrap();
}
