#![feature(plugin)]
#![plugin(rocket_codegen)]

extern crate rocket;

#[get("/")]
fn index() -> &'static str {
    "<h1> Hello, world! </h1>"
}

fn main() {
    rocket::ignite().mount("/", routes![index]).launch();
}