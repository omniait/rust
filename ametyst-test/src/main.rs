extern crate amethyst;

use amethyst::prelude::*;
use amethyst::renderer::{DisplayConfig, DrawFlat2D, Event, Pipeline,
                         RenderBundle, Stage, VirtualKeyCode};

pub struct Pong;

impl SimpleState for Pong {

}

fn main() -> amethyst::Result<()> {
    amethyst::start_logger(Default::default());

    // RGBA base 255, 32 bit precision
    let mut colours: [f32; 4] = [41.0, 49.0, 51.0, 255.0];
    // To base 1
    for i in 0..colours.len() {
        colours[i] = colours[i] / 255.0;
    }


    use amethyst::utils::application_root_dir;

    let path = format!("{}/resources/display_config.ron", application_root_dir());

    let config = DisplayConfig::load(&path);

    let pipe = Pipeline::build()
        .with_stage(
            Stage::with_backbuffer()
                .clear_target(colours, 1.0)
                .with_pass(DrawFlat2D::new()),
        );
        
    let game_data = GameDataBuilder::default()
        .with_bundle(
        RenderBundle::new(pipe, Some(config))
            .with_sprite_sheet_processor()
        )?;

    let mut game = Application::new("./", Pong, game_data)?;

    game.run();


    Ok(())
}
